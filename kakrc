# Source plugin manager
source "~/.config/kak/plugins/plug.kak/rc/plug.kak"
# Load plugins
plug "https://github.com/andreyorst/plug.kak"
plug "https://github.com/insipx/kak-crosshairs"
plug "andreyorst/powerline.kak" config %{
        powerline-start
}
plug 'JJK96/kakoune-repl-send' %{
      # Suggested mapping
        map global normal <backspace> ': repl-send<ret>'
}
# Manually installed and sourced plugins
source "~/.config/kak/plugins/prelude.kak/rc/prelude.kak"
source "~/.config/kak/plugins/auto-pairs.kak/rc/auto-pairs.kak"
# Enable language server protocol
eval %sh{kak-lsp --kakoune -s $kak_session}
lsp-enable
# Gruvbox colorscheme
colorscheme gruvbox
# Relative Numberlines
add-highlighter global/ number-lines -relative
# Cursor crosshairs
crosshairs
# Powerline, Clippy etc on Top
set-option -add global ui_options ncurses_status_on_top=yes
#Make Clippy a cute kitty :)
set-option -add global ui_options ncurses_assistant=cat
# Stop Kakoune changing the XRoot Header (For DWM and similar window managers where the bar displays that.)
set-option -add global ui_options ncurses_set_title=no
# Indentation and Tabstop
set-option global tabstop 4
set-option global indentwidth 4
# Case insensitive search
map global prompt <a-i> "<home>(?i)<end>"
# Clipboard interaction
map global user p -docstring 'paste from clipboard' '!xsel -bo<ret>uU'
map global user y -docstring 'copy to clipboard' '<a-|>xsel -bi<ret>'
map global user d -docstring 'cut to clipboard' '|xsel -bi<ret>'
# Add R REPL support
hook global WinSetOption filetype=R %{
    set window repl_send_command "R --interactive"
    set window repl_send_exit_command "exit()"
}
