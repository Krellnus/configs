" Use intuitive over traditional VI mappings
set nocompatible
" Plug-ins managed with Vim-Plug
call plug#begin('~/local/share/nvim/plugged')
" Faster code folding
Plug 'https://github.com/Konfekt/FastFold'
" You Complete Me Autocompleter
Plug 'https://github.com/ycm-core/YouCompleteMe'
" Vim Powerline using Airline
Plug  'https://github.com/vim-airline/vim-airline'
" R plugin for vim
Plug 'https://github.com/jalvesaq/Nvim-R' 
" Synchronous auto completion for R
Plug 'https://github.com/gaalcaras/ncm-R'
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
"Automatic closing of delimiters
Plug 'https://github.com/Raimondi/delimitMate'
" Plugin for auto previewing LaTeX documents
Plug 'https://github.com/xuhdev/vim-latex-live-preview', { 'for': 'tex' }
"Vimwiki plugin
Plug 'vimwiki/vimwiki'
"Gruvbox theme
Plug 'morhetz/gruvbox'
"Comment code
Plug 'https://github.com/tpope/vim-commentary'
" Syntax Highlighting for i3 config files. 
Plug 'https://github.com/PotatoesMaster/i3-vim-syntax'
" Syntax Highlighting and indentation management for Haskell files
Plug 'https://github.com/neovimhaskell/haskell-vim'
" Syntax folding for Python code
Plug 'https://github.com/tmhedberg/SimpylFold'
" Better indentation for Python
Plug 'vim-scripts/indentpython.vim'
" Python REPL
Plug 'https://github.com/sillybun/vim-repl'
call plug#end()
 
" Determine file type based on metadata where possible
filetype indent plugin on
 
" Enable syntax highlighting
syntax on

" Hide buffers not in use by the current window.
set hidden
 
" Better command-line completion
set wildmode=longest,list,full
 
" Show partial commands in the last line of the screen
set showcmd
 
" Highlight searches, <C-l> clears. 
set hlsearch
 
"Ignore case when searching
set ignorecase

"Unless capital letters are used.
set smartcase
 
" When opening a new file with unknown indentation, keep the same indentation.
set autoindent
 
" Stop certain movements from always going to the first character of a line.
set nostartofline
 
" Display the cursor position on the last line of the screen or in the status  line of a window
set ruler
 
" Always display the status line, even if only one window is displayed
set laststatus=2
 
" Ask to confirm instead of throwing an error.
set confirm
 
" Use visual bell instead of beeping when doing something wrong
set visualbell
 
"If visualbell is set, and this line is also included, vim will neither flash nor beep. If visualbell is unset, this does nothing.
set t_vb=
 
" Enable use of the mouse for all modes
set mouse=a
 
" Set the command window height to 2 lines, to avoid many cases of having to "press <Enter> to continue"
set cmdheight=2
 
" Display relative line numbers on the left
set number relativenumber
 
" Quickly time out on keycodes, but never time out on mappings
set notimeout ttimeout ttimeoutlen=200
 
" Use <F11> to toggle between 'paste' and 'nopaste'
set pastetoggle=<F11>
 
 
" Indentation settings for using 4 spaces instead of tabs do not change 'tabstop' from its default value of 8 with this setup.
set shiftwidth=4
set softtabstop=4
set expandtab
 
" Enable code folding
set foldmethod=indent
set foldlevel=99

" Map Y to act like D and C, i.e. to yank until EOL, rather than act as yy,  which is the default
map Y y$
 
" Map <C-L> (redraw screen) to also turn off search highlighting until the next search
nnoremap <C-L> :nohl<CR><C-L>

"Set up cursor crosshair
set cursorline
set cursorcolumn

" Set the colour scheme of Vim
colorscheme gruvbox
set background=dark

" Fix splitting
set splitbelow splitright

" Just use C-motion to move instead of polling with <C+w>.
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l
nnoremap <C-h> <C-W>h

" Use system clipboard
set clipboard=unnamedplus

"Fix tex file types
" autocmd BufRead,BufNewFile *.tex set filetype=tex<CR>
" autocmd FileType tex,latex,markdown setlocal spell spelllang=en_au<CR>

" Set latex engine for LLP
let g:tex_flavor='latex'
" Allow LLP to read latex documents quickly
autocmd Filetype tex, setl updatetime=10

" Auto center when entering insert mode
autocmd InsertEnter * norm zz <CR>

" Remove trailing whitespace          
autocmd BufWritePre *s/\s\+%//e <CR>

" Keybound commands
let mapleader=" "

" Compile the document
map <leader>c :w \| !compiler.sh <c-r>%<CR>

" Enable spell checking for spell check
map <leader>s :setlocal spell! spelllang-en_au<CR>

" Enable/Disable auto indentation
map <leader>i :setlocal autoindent<CR>
map <leader>I :setlocal autoindent<CR>

" Fix Backspace (st only)
set backspace=indent,eol,start
:fixdel

" Command is :LLPStartPreview
let g:livepreview_previewer = 'zathura'

" let &t_EI = "\<Esc>[2 q"
" Disable auto commenting by default
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o 

" NCM2
autocmd BufEnter * call ncm2#enable_for_buffer()    " To enable ncm2 for all buffers.
set completeopt=noinsert,menuone,noselect           " :help Ncm2PopupOpen for more
                                                    " information.
" Close autopreview in ycm
let g:ycm_autoclose_preview_window_after_completion=1
" Go to definition declarations
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>


 " ____  _   _ _____ _     _ 
" / ___|| | | | ____| |   | | 
" \___ \| |_| |  _| | |   | | 
 " ___) |  _  | |___| |___| |___ 
" |____/|_| |_|_____|_____|_____| 

"Skeleton file
autocmd BufNewFile *.sh 0r ~/.vim/templates/skeleton.sh

 " ____  __  __            _       _ 
" |  _ \|  \/  | __ _ _ __| | ____| | _____      ___ __ 
" | |_) | |\/| |/ _` | '__| |/ / _` |/ _ \ \ /\ / / '_ \ 
" |  _ <| |  | | (_| | |  |   < (_| | (_) \ V  V /| | | | 
" |_| \_\_|  |_|\__,_|_|  |_|\_\__,_|\___/ \_/\_/ |_| |_| 

" Skeleton file
autocmd BufNewFile *.Rmd 0r ~/.vim/templates/skeleton.Rmd

 " _   _           _        _ _ 
" | | | | __ _ ___| | _____| | | 
" | |_| |/ _` / __| |/ / _ \ | | 
" |  _  | (_| \__ \   <  __/ | | 
" |_| |_|\__,_|___/_|\_\___|_|_| 

" " Various syntax highlights for Haskell
let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords


 " ____        _   _ 
" |  _ \ _   _| |_| |__   ___  _ __ 
" | |_) | | | | __| '_ \ / _ \| '_ \ 
" |  __/| |_| | |_| | | | (_) | | | | 
" |_|    \__, |\__|_| |_|\___/|_| |_| 
 "       |___/ 

" Set proper PEP 8 indentation
au BufNewFile,BufRead *.py
    \ set tabstop=4
    \ set softtabstop=4
    \ set shiftwidth=4
    \ set textwidth=79
    \ set expandtab
    \ set autoindent
    \ set fileformat=unix

" Activate REPL (Also supports PERL and Vimscript)
nnoremap <leader>r :REPLToggle
