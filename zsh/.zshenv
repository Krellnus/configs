#!/bin/zsh

#Exporting to the path

#Usefuls Dirs
export PATH="/home/stuartf/bin:$PATH"
export PATH="/home/stuartf/bin/scripts:$PATH"
export PATH="/home/stuartf/bin/statusbar:$PATH"

#Default programs
export EDITOR="/usr/bin/vim"
export BROWSER="/usr/bin/brave"
export READER="/usr/bin/zathura"
export TERMINAL="st"

#Tidying up home
export XDG_CONFIG_HOME="$HOME/.config"
export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
